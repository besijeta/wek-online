from .models import CSResource
import django_tables2 as tables

class CSResourceTable(tables.Table):

  class Meta:
    model = CSResource
    attrs = {"class": "paleblue"}
    fields = ("name", "description", "platforms", )

  platforms = tables.Column(accessor='platforms', orderable=False, verbose_name='platform')
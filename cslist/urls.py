from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'csresource-table/', views.CSResourceTableView.as_view(), name="csresource_table"),
]
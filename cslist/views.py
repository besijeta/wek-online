from django.shortcuts import render
from .models import CSResource, Platform
from .tables import CSResourceTable
from django_tables2 import RequestConfig
from django.views.generic.base import TemplateView
from django.db.models import Q
 
 # Create your views here.
def cslist_context_data(context, request):
  objects = []
  platform_filters = []

  query = Q()
 
  if 'platform_filter' in request.GET:
    platform_filters = request.GET['platform_filter'].split(",")

  if len(platform_filters) > 0:
    
    for platform in platform_filters:
      query = query | Q(platform__name = platform)

  objects = CSResource.objects.filter(query)
  table = CSResourceTable(objects)
  RequestConfig(request).configure(table)
  context["table"] = table

  context["platform_name_choices"] = Platform.PLATFORM_NAME_CHOICES

class CSResourceTableView(TemplateView):
  template_name = "cslist/cslist.html"

  def get_context_data(self, **kwargs):
    context = super(CSResourceTableView, self).get_context_data(**kwargs)
    cslist_context_data(context, self.request)
    return context
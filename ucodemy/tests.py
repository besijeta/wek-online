from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

USER_NAME = 'user'
USER_EMAIL = 'user@test.com'
USER_PASSWORD = 'pass'

class HomePageTest(TestCase):

  def setup(self):
    pass

  def login_user(self):
    User.objects.create_user(USER_NAME, USER_EMAIL, USER_PASSWORD)
    self.client.login(username=USER_NAME, password=USER_PASSWORD)

  def test_signed_out_display_jumbotron(self):
    response = self.client.get(reverse('home'))
    self.assertContains(response, "jumbotron")

  def test_signed_in_doesnt_display_jumbotron(self):
    self.login_user()
    response = self.client.get(reverse('home'), follow=True)
    self.assertNotContains(response, "jumbotron")